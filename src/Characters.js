import React from "react";
import { gql } from "apollo-boost";
import { Query } from "react-apollo";
import Loading from "./Loading";

/*
  1) Query the api to return an array of character data.
  2) Create a 3 column grid tht displays an item with:
    - a thumbnail of the character
    - the character name
    - the character status
    - the character species
  3) Create a media query that displays a list of all characters in a single column if the browser width is below 768px

  graphql api: https://rickandmortyapi.com/graphql
*/

const ALL_CHARACTERS = gql`
  query AllCharacters {
    characters {
      results {
        id
        name
        species
        status
        image
      }
    }
  }
`;

export default () => (
  <Query query={ALL_CHARACTERS}>
    {({ loading, error, data :{characters} }) => {
      if (loading) return <Loading />;
      if (error) return `Error! ${error.message}`;

      return characters && characters.results.map(el => {
        return(<div className="characters-container">
          <img className="characters-img" src={el.image} alt="" />
          <p className="characters-name">{el.name}</p>
          <p>{el.species}</p>
          <p>{el.status}</p>
        </div>)
      });
    }}
  </Query>
);
