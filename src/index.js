import React from "react";
import ReactDOM from "react-dom";
import Main from "./App";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";

import "./styles.css";

// Pass your GraphQL endpoint to uri
const client = new ApolloClient({
  uri: "https://rickandmortyapi.com/graphql"
});

const App = () => (
  <ApolloProvider client={client}>
    <Main />
  </ApolloProvider>
);

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
