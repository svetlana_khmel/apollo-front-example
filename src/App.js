import React from "react";
import Characters from "./Characters";

export default () => (
  <div className="App">
    <h1>Character Roster</h1>
    <Characters />
  </div>
);
